package pl.maciejdados.ordermanager.unit.utils;

import org.junit.Test;
import pl.maciejdados.ordermanager.dto.OrderXmlDto;
import pl.maciejdados.ordermanager.entity.Order;
import pl.maciejdados.ordermanager.utils.Xml;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

/**
 * Unit tests of {@link pl.maciejdados.ordermanager.utils.Xml}
 */
public class XmlUnitTest {
    private Xml xml = new Xml();
    private final String output =
            "<requests>" +
            "<request>" +
            "<clientId>1</clientId>" +
            "<requestId>1</requestId>" +
            "<name>Bułka</name>" +
            "<quantity>1</quantity>" +
            "<price>10.0</price>" +
            "</request>" +
            "</requests>";

    private final String input =
            "<requests>" +
            "<request>" +
            "<clientId>1</clientId>" +
            "<requestId>1</requestId>" +
            "<name>Bułka</name>" +
            "<quantity>1</quantity>" +
            "<price>10.00</price>" +
            "</request>" +
            "</requests>";

    @Test
    public void givenProperDto_whenSerialize_thenSerializedProperly() throws Exception {
        final OrderXmlDto dto = new OrderXmlDto("1", 1, "Bułka", 1, 10.0);
        assertThat(xml.serialize(Collections.singletonList(dto)), equalTo(output));
    }

    @Test
    public void givenProperString_whenDeserialize_thenProperOrderAndNoExcetion() throws Exception {
        final List<Order> deserialized = xml.deserialize(input);
        final Order order = new Order("1",1,"Bułka", 1, 10.0);

        assertThat(deserialized, hasSize(1));
        assertThat(deserialized, hasItem(order));
    }

    @Test
    public void givenInvalidStringNotXml_whenDeserialize_thenEmptyList() {
        final List<Order> deserialized = xml.deserialize("bad input\nhas at least two lines\nso its not skipped\nawd\n");
        assertThat(deserialized, hasSize(0));
    }

    @Test
    public void givenInvalidStringIsXml_whenDeserialize_thenEmptyList() {
        final String invalidXml = input.replace("10.00", "10.awd"); // make invalid xml
        final List<Order> deserialized = xml.deserialize(invalidXml);
        assertThat(deserialized, hasSize(0));
    }

    @Test
    public void givenEmptyString_whenDeserialize_thenEmptyList() {
        final List<Order> deserialized = xml.deserialize("");
        assertThat(deserialized, hasSize(0));
    }
}
