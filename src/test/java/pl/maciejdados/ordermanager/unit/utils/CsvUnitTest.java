package pl.maciejdados.ordermanager.unit.utils;

import org.junit.Test;
import pl.maciejdados.ordermanager.dto.OrderCsvDto;
import pl.maciejdados.ordermanager.entity.Order;
import pl.maciejdados.ordermanager.utils.Csv;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

/**
 * Unit tests of {@link Csv} class
 */
public class CsvUnitTest {
    private Csv csv = new Csv();
    private final String serialized = "clientId,requestId,name,quantity,price\n1,1,Bułka,1,10.0\n";

    @Test
    public void givenProperDto_whenSerialize_thenSerializedProperly() throws Exception {
        final OrderCsvDto dto = new OrderCsvDto("1", 1, "Bułka", 1, 10.0);
        assertThat(csv.serialize(Collections.singletonList(dto)), equalTo(serialized));
    }

    @Test
    public void givenProperString_whenDeserialize_thenProperOrderAndNoExcetion() throws Exception {
        final List<Order> deserialized = csv.deserialize(serialized);
        final Order order = new Order("1",1,"Bułka", 1, 10.0);

        assertThat(deserialized, hasSize(1));
        assertThat(deserialized, hasItem(order));
    }

    @Test
    public void givenInvalidStringNotCsv_whenDeserialize_thenEmptyList() {
        final List<Order> deserialized = csv.deserialize("bad input\nhas at least two lines\nso its not skipped\nawd\n");
        assertThat(deserialized, hasSize(0));
    }

    @Test
    public void givenInvalidStringIsCsv_whenDeserialize_thenEmptyList() {
        final String invalidCsv = "clientId,requestId,name,quantity,price\n1,1asdf,Bułka,1asdf,10.0\n";
        final List<Order> deserialized = csv.deserialize(invalidCsv);
        assertThat(deserialized, hasSize(0));
    }

    @Test
    public void givenEmptyString_whenDeserialize_thenEmptyList() {
        final List<Order> deserialized = csv.deserialize("");
        assertThat(deserialized, hasSize(0));
    }
}
