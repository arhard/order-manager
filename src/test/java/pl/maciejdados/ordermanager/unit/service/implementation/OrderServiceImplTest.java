package pl.maciejdados.ordermanager.unit.service.implementation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.maciejdados.ordermanager.entity.Order;
import pl.maciejdados.ordermanager.repository.OrderRepository;
import pl.maciejdados.ordermanager.service.implementation.OrderServiceImpl;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class OrderServiceImplTest {
    @MockBean
    private OrderRepository orderRepository;

    private OrderServiceImpl orderService;

    @Before
    public void setup() {
        orderService = new OrderServiceImpl(orderRepository);
    }

    @Test
    public void givenOrder_whenSave_thenOrderReturned() {
        final Order order = new Order("1", 1, "Chleb", 1, 10.0);

        when(orderRepository.save(order)).thenReturn(order);

        assertThat(orderService.save(order), equalTo(order));
    }


    @Test
    public void givenTwoOrdersExist_whenTotalOrderCount_thenTwoReturned() {
        when(orderRepository.countAll()).thenReturn(2L);
        assertThat(orderService.totalOrderCount(), equalTo(2L));
    }

    @Test
    public void givenTwoOrdersExistForAClient_whenOrderCountByClientId_thenTwoReturned() {
        when(orderRepository.countAllByClientId("1")).thenReturn(2L);
        assertThat(orderService.orderCountByClientId("1"), equalTo(2L));
    }

    @Test
    public void givenTotalOrderPrice10_whenTotalOrderPrice_then10Returned() {
        when(orderRepository.totalOrderPrice()).thenReturn(10.0);
        assertThat(orderService.totalOrderPrice(), equalTo(10.0));
    }

    @Test
    public void givenTotalOrderPrice10ForAClient_whenTotalOrderPriceByClientId_then10Returned() {
        when(orderRepository.totalOrderPriceByClientId("1")).thenReturn(10.0);
        assertThat(orderService.orderPriceByClientId("1"), equalTo(10.0));
    }


    @Test
    public void givenTwoOrders_whenFindAllOrders_twoOrdersReturned() {
        final List<Order> orders = Arrays.asList(
                new Order("1", 1, "Chleb", 1, 10.0),
                new Order("1", 2, "Chleb", 1, 10.0)
        );
        when(orderRepository.findAll()).thenReturn(orders);
        assertThat(orderService.findAllOrders(), hasSize(2));
        assertThat(orderService.findAllOrders(), equalTo(orders));
    }

    @Test
    public void givenTwoOrdersForAClient_whenFindAllOrdersByClientId_twoOrdersReturned() {
        final List<Order> orders = Arrays.asList(
                new Order("1", 1, "Chleb", 1, 10.0),
                new Order("1", 2, "Chleb", 1, 10.0)
        );
        when(orderRepository.findAllByClientId("1")).thenReturn(orders);
        assertThat(orderService.findOrdersByClientId("1"), hasSize(2));
        assertThat(orderService.findOrdersByClientId("1"), equalTo(orders));
    }

    @Test
    public void givenAverageOrderPrice10_whenAverageOrderPrice_then10Returned() {
        when(orderRepository.averageOrderPrice()).thenReturn(10.0);
        assertThat(orderService.averageOrderPrice(), equalTo(10.0));
    }

    @Test
    public void givenAverageOrderPrice10ForAClient_whenAverageOrderPrice_then10Returned() {
        when(orderRepository.averageOrderPriceByClientId("1")).thenReturn(10.0);
        assertThat(orderService.averageOrderPriceByClientId("1"), equalTo(10.0));

    }
}
