package pl.maciejdados.ordermanager.integration;


import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import pl.maciejdados.ordermanager.controller.OrderManagerController;
import pl.maciejdados.ordermanager.entity.Order;
import pl.maciejdados.ordermanager.repository.OrderRepository;
import pl.maciejdados.ordermanager.service.StdinService;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class OrderControllerIntegrationTest {

    @SpyBean
    private OrderManagerController controller;

    @MockBean
    private StdinService stdinService;

    @Autowired
    private OrderRepository orderRepository;

    private List<Order> orders;
    private static final String filename = "test.csv";


    @Before
    public void setup() {
        orders = controller.loadOrdersFromFiles(new String[]{"data.csv", "data.xml"});
    }

    @After
    public void tearDown() {
        orders.clear();
        orderRepository.deleteAll();
    }

    @AfterClass
    public static void deleteTestFiles() {
        try {
            File f = new File(filename);
            if(f.delete()) {
                System.out.println("File deleted.");
            }
        } catch (Exception ex) {
            System.err.println("Error occurred during test file deletion");
        }
    }

    @Test
    public void givenFilesExist_whenLoadOrdersFromFiles_thenOrdersLoaded() {
        final List<Order> expectedOrders = Arrays.asList(
          new Order("1", 1, "Bułka", 1, 10.0),
          new Order("1", 1, "Chleb", 2, 15.0),
          new Order("1", 2, "Chleb", 5, 15.0),
          new Order("2", 1, "Chleb", 1, 10.0),
          new Order("1", 1, "Bułka", 1, 10.0),
          new Order("1", 1, "Chleb", 2, 15.0),
          new Order("1", 2, "Chleb", 5, 15.0),
          new Order("2", 1, "Chleb", 1, 10.0)
        );

        assertThat(orders, hasSize(8));
//        assertThat(oders, equalTo(orders));
    }

    @Test
    public void givenOptionWasChosen_whenMenu_thenChoseRightOption() {
        when(stdinService.readString()).thenReturn("a", "exit");

        controller.menu();

        verify(controller, times(1)).totalOrderCount();
    }

    @Test
    public void givenInvalidFileExtension_whenLoadOrdersFromFiles_thenOrdersEmpty() {
        orders.clear();
        orders = controller.loadOrdersFromFiles(new String[]{"test.txt"});
        assertThat(orders, hasSize(0));
    }

    @Test
    public void givenUserWantsToSaveReport_whenSaveDialogPrompt_thenReturnFilename() {
        when(stdinService.readString()).thenReturn("y", "test.csv");
        assertThat(controller.saveDialogPrompt(), equalTo("test.csv"));
    }

    @Test
    public void givenUserDoesntWantToSaveReport_whenSaveDialogPrompt_thenReturnEmpty() {
        when(stdinService.readString()).thenReturn("n", "test.csv");
        assertThat(controller.saveDialogPrompt(), equalTo(""));
    }

    @Test
    public void givenEmptyFilename_whenSaveReport_thenNoFileSaved() {
        assertThat(controller.saveReport("", "test", "test"), equalTo(""));
    }

    @Test
    public void givenFilenameAndHeaderAndData_whenSaveReport_thenFileContentMatches() throws Exception {
        String filename = controller.saveReport("test.csv", "header", 10L);

        File file = new File(filename);
        String fileContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        assertThat(fileContent, equalTo("header\n10"));

    }

    @Test
    public void givenFilenameAndHeaderAndData_whenSaveReport_thenFileSaved() throws Exception {
        String filename = controller.saveReport("test.csv", "header", 10L);

        File file = new File(filename);
        assertTrue(file.exists() && !file.isDirectory());

    }

    @Test
    public void givenNoFilenameAndOrderList_whenSaveReport_thenNoFileSaved() {
        assertThat(controller.saveReport("", orders), equalTo(""));
    }


    @Test
    public void givenFilenameAndOrderList_whenSaveReport_thenFileSaved() {
        String filename = controller.saveReport("test.csv", orders);
        File file = new File(filename);
        assertTrue(file.exists() && !file.isDirectory());

    }

    @Test
    public void givenFilenameAndOrderList_whenSaveReport_thenFileContentMatches() throws Exception {
        String filename = controller.saveReport("test.csv", Arrays.asList(new Order("1", 1, "test", 1, 10.0)));

        File file = new File(filename);
        String fileContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        assertThat(fileContent, equalTo("clientId,requestId,name,quantity,price\n1,1,test,1,10.0\n"));
    }


    @Test
    public void givenClientIdIsTyped_whenClientIdPrompt_thenClientIdIsReturned() {
        when(stdinService.readString()).thenReturn("1");
        assertThat(controller.clientIdPrompt(), equalTo("1"));
    }

    @Test
    public void givenOrders_whenTotalOrderCount_thenOrderCountSavedToFile() throws Exception {
        when(stdinService.readString()).thenReturn("y", "test.csv");
        controller.totalOrderCount();

        File file = new File(filename);
        String fileContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        assertThat(fileContent, equalTo("total_order_count\n8"));
    }

    @Test
    public void givenOrders_whenTotalOrderCountById_thenOrderCountSavedToFile() throws Exception {

        when(stdinService.readString()).thenReturn("1", "y", "test.csv");
        controller.totalOrderCountByClientId();

        File file = new File(filename);
        String fileContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        assertThat(fileContent, equalTo("clientId,total_order_count\n1,6.0"));
    }

    @Test
    public void givenNoOrders_whenTotalOrderPrice_thenSavedToFile() throws Exception {
        when(stdinService.readString()).thenReturn("y", "test.csv");
        orderRepository.deleteAll();

        controller.totalOrderPrice();

        File file = new File(filename);
        String fileContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        assertThat(fileContent, equalTo("total_order_price\n0.0"));
    }

    @Test
    public void givenOrders_whenTotalOrderPrice_thenSavedToFile() throws Exception {
        when(stdinService.readString()).thenReturn("y", "test.csv");

        controller.totalOrderPrice();

        File file = new File(filename);
        String fileContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        assertThat(fileContent, equalTo("total_order_price\n100.0"));
    }

    @Test
    public void givenOrders_whenTotalOrderPriceById_thenSavedToFile() throws Exception {
        when(stdinService.readString()).thenReturn("1", "y", "test.csv");

        controller.totalOrderPriceByClientId();

        File file = new File(filename);
        String fileContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        assertThat(fileContent, equalTo("clientId,total_order_price\n1,80.0"));
    }

    @Test
    public void givenOrders_whenFindAllOrders_thenOrdersSavedToFile() throws Exception {
        when(stdinService.readString()).thenReturn("y", "test.csv");

        controller.findAllOrders();

        File file = new File(filename);
        String fileContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        assertThat(fileContent, equalTo("clientId,requestId,name,quantity,price\n1,1,Bułka,1,10.0\n1,1,Chleb,2,15.0\n1,2,Chleb,5,15.0\n2,1,Chleb,1,10.0\n1,1,Bułka,1,10.0\n1,1,Chleb,2,15.0\n1,2,Chleb,5,15.0\n2,1,Chleb,1,10.0\n"));
    }
    @Test
    public void givenOrders_whenFindAllOrdersByClientId_thenOrdersSavedToFile() throws Exception {
        when(stdinService.readString()).thenReturn("2","y", "test.csv");

        controller.findAllOrdersByClientId();

        File file = new File(filename);
        String fileContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        assertThat(fileContent, equalTo("clientId,requestId,name,quantity,price\n2,1,Chleb,1,10.0\n2,1,Chleb,1,10.0\n"));
    }

    @Test
    public void givenOrders_whenAverageOrderPrice_thenAveragePriceSavedToFile() throws Exception {
        when(stdinService.readString()).thenReturn("y", "test.csv");

        controller.averageOrderPrice();

        File file = new File(filename);
        String fileContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        assertThat(fileContent, equalTo("average_order_price\n12.5"));

    }

    @Test
    public void givenOrders_whenAverageOrderPriceByClientId_thenAveragePriceSavedToFile() throws Exception {
        when(stdinService.readString()).thenReturn("2","y", "test.csv");

        controller.averageOrderPriceByClientId();

        File file = new File(filename);
        String fileContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        assertThat(fileContent, equalTo("clientId,average_order_price\n2,10.0"));

    }
}


