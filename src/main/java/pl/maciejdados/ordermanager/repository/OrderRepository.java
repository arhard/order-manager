package pl.maciejdados.ordermanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.maciejdados.ordermanager.entity.Order;

import java.util.List;

/**
 * Spring data repository used for data access
 *
 * @author Maciej Dados
 */
public interface OrderRepository extends JpaRepository<Order, Long> {

    /**
     * Counts all orders
     * @return order count
     */
    @Query("SELECT COUNT(o) FROM Order o")
    long countAll();

    /**
     * Counts all orders assigned to a customer
     * @param clientId client's id
     * @return client's order count
     */
    long countAllByClientId(String clientId);

    /**
     * Sums prices of all orders
     * @return total price of orders
     */
    @Query("SELECT SUM(o.price) FROM Order o")
    double totalOrderPrice();


    /**
     * Sums price of all orders of a particular client
     * @param clientId client's id
     * @return sum of all orders of a particular client
     */
    @Query("SELECT SUM(o.price) FROM Order o WHERE o.clientId = ?1")
    double totalOrderPriceByClientId(String clientId);


    /**
     * Finds all orders in a db
     * @return list of all orders from db
     */
    List<Order> findAll();

    /**
     * Find all orders assigned to a particular client
     * @param clientId client's id
     * @return list of all orders assigned to a particular client
     */
    List<Order> findAllByClientId(String clientId);


    /**
     * Computes average price of all orders in db
     * @return average price of all orders in db
     */
    @Query("SELECT AVG(o.price) FROM Order o")
    double averageOrderPrice();


    /**
     * Computes average price of all orders assigned to a client
     * @param clientId client's id
     * @return average price of all orders assigned to a client
     */
    @Query("SELECT AVG(o.price) FROM Order o WHERE o.clientId = ?1")
    double averageOrderPriceByClientId(String clientId);
}
