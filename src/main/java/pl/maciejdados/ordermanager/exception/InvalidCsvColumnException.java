package pl.maciejdados.ordermanager.exception;

/**
 * Exception thrown when parsing CSV file and column count doesn't match
 */
public class InvalidCsvColumnException extends RuntimeException {
    public InvalidCsvColumnException(String message) {
        super(message);
    }
}
