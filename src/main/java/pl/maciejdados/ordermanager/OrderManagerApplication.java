package pl.maciejdados.ordermanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Application's entry point
 *
 * @author Maciej Dados
 */
@SpringBootApplication
public class OrderManagerApplication {


	public static void main(String[] args) {
		System.out.println("Loading...");
		SpringApplication.run(OrderManagerApplication.class, args);
	}

}
