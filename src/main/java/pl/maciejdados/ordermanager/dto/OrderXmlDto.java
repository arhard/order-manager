package pl.maciejdados.ordermanager.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.maciejdados.ordermanager.entity.Order;


/**
 * Order dto used when dealing with xml document processing
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JacksonXmlRootElement(localName = "request")
public class OrderXmlDto {
    private String clientId;
    private long requestId;
    private String name;
    private int quantity;
    private double price;

    /**
     * Coverts {@link OrderXmlDto} to  {@link Order}
     * @param dto {@link OrderXmlDto} that will be converted to {@link Order}
     * @return {@link Order} object
     */
    public static Order toOrder(OrderXmlDto dto) {
        return new Order(dto.clientId, dto.requestId, dto.name, dto.quantity, dto.price);
    }
}
