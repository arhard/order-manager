package pl.maciejdados.ordermanager.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.maciejdados.ordermanager.entity.Order;


/**
 * Order dto used when dealing with csv document processing
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"clientId", "requestId", "name", "quantity", "price"})
public class OrderCsvDto {
    public static final int COLUMN_COUNT = 5;
    private String clientId;
    private long requestId;
    private String name;
    private int quantity;
    private double price;

    /**
     * Coverts {@link OrderCsvDto} to  {@link Order}
     * @param dto {@link OrderCsvDto} that will be converted to {@link Order}
     * @return {@link Order} object
     */
    public static Order toOrder(OrderCsvDto dto) {
        return new Order(dto.clientId, dto.requestId, dto.name, dto.quantity, dto.price);
    }
}
