package pl.maciejdados.ordermanager.service;

public interface StdinService {
    String readString();
}
