package pl.maciejdados.ordermanager.service.implementation;

import org.springframework.stereotype.Service;
import pl.maciejdados.ordermanager.service.StdinService;

import java.util.Scanner;

@Service
public class StdinServiceImpl implements StdinService {
    private final Scanner scanner = new Scanner(System.in);

    @Override
    public String readString() {
        return scanner.next();
    }
}
