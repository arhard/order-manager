package pl.maciejdados.ordermanager.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.maciejdados.ordermanager.entity.Order;
import pl.maciejdados.ordermanager.repository.OrderRepository;
import pl.maciejdados.ordermanager.service.OrderService;

import java.util.List;

/**
 * Implementation of OrderService
 *
 * @author Maciej Dados
 */
@Service
public class OrderServiceImpl implements OrderService {

    // injected repository
    private final OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Order save(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public long totalOrderCount() {
        return orderRepository.countAll();
    }

    @Override
    public long orderCountByClientId(String clientId) {
        return orderRepository.countAllByClientId(clientId);
    }

    @Override
    public double totalOrderPrice() {
        try {
            return orderRepository.totalOrderPrice();
        } catch (Exception ex) { // when no rows in table and using sql aggregation such as SUM spring throws exception
            return 0.0;
        }
    }

    @Override
    public double orderPriceByClientId(String clientId) {
        return orderRepository.totalOrderPriceByClientId(clientId);
    }

    @Override
    public List<Order> findAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> findOrdersByClientId(String clientId) {
        return orderRepository.findAllByClientId(clientId);
    }

    @Override
    public double averageOrderPrice() {
        return orderRepository.averageOrderPrice();
    }

    @Override
    public double averageOrderPriceByClientId(String clientId) {
        return orderRepository.averageOrderPriceByClientId(clientId);
    }
}
