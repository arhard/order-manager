package pl.maciejdados.ordermanager.service;

import pl.maciejdados.ordermanager.entity.Order;

import java.util.List;

/**
 * Defines available actions on orders
 *
 * @author Maciej Dados
 */
public interface OrderService {

    Order save(Order order);

    /**
     * Total number of orders in a database
     * @return order count
     */
    long totalOrderCount();

    /**
     * Number of orders assigned to a particular client
     * @param clientId client's id
     * @return order count
     */
    long orderCountByClientId(String clientId);

    /**
     * Total price of all orders
     * @return total price
     */
    double totalOrderPrice();


    /**
     * Price of all orders assigned to a particular client
     * @param clientId client's id
     * @return price of client's orders
     */
    double orderPriceByClientId(String clientId);

    /**
     * Finds all orders
     * @return list of all orders
     */
    List<Order> findAllOrders();

    /**
     * Finds orders assigned to a particular client
     * @param clientId client's id
     * @return list of orders of a particular client
     */
    List<Order> findOrdersByClientId(String clientId);

    /**
     * Find average price of all orders
     * @return average price
     */
    double averageOrderPrice();

    /**
     * Find average price of orders of a particular client
     * @param clientId client's id
     * @return average price
     */
    double averageOrderPriceByClientId(String clientId);
}
