package pl.maciejdados.ordermanager.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;


/**
 * Class describing order
 *
 * @author Maciej Dados
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "\"ORDER\"")
public class Order {

    @Id
    @GeneratedValue
    @Column(name = "ORDER_ID")
    @JsonIgnore
    @ToString.Exclude
    private long orderId = 0;

    @Column(name = "CLIENT_ID")
    private String clientId;

    @Column(name = "REQUEST_ID")
    private long requestId;

    private String name;

    @Column(name = "QUANTITY")
    private int quantity;

    @Column(name = "PRICE")
    private double price;

    public Order(String clientId, long requestId, String name, int quantity, double price) {
        this.clientId = clientId;
        this.requestId = requestId;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }
}
