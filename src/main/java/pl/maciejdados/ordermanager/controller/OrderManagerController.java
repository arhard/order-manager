package pl.maciejdados.ordermanager.controller;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.maciejdados.ordermanager.entity.Order;
import pl.maciejdados.ordermanager.service.OrderService;
import pl.maciejdados.ordermanager.service.StdinService;
import pl.maciejdados.ordermanager.utils.Mapper;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class OrderManagerController {
    private final Logger logger = LoggerFactory.getLogger(OrderManagerController.class);

    private final StdinService stdinService;

    private final OrderService orderService;
    private final List<Order> orders = new ArrayList<>();

    private final Mapper xmlMapper;
    private final Mapper csvMapper;

    @Autowired
    public OrderManagerController(StdinService stdinService, OrderService orderService, @Qualifier("XmlMapper") Mapper xmlMapper, @Qualifier("CsvMapper")Mapper csvMapper) {
        this.stdinService = stdinService;
        this.orderService = orderService;
        this.xmlMapper = xmlMapper;
        this.csvMapper = csvMapper;
    }

    public List<Order> loadOrdersFromFiles(String[] fileNames) {
        Arrays.stream(fileNames).forEach(arg -> {
            try {
                if (arg.endsWith(".csv")) {
                    orders.addAll(csvMapper.deserialize(FileUtils.readFileToString(new File(arg), StandardCharsets.UTF_8)));
                } else if(arg.endsWith(".xml")) {
                    orders.addAll(xmlMapper.deserialize(FileUtils.readFileToString(new File(arg), StandardCharsets.UTF_8)));
                } else {
                    System.out.println("Unknown file extension in file name: " + arg);
                }
            } catch (Exception e) {
                logger.warn(String.format("Exception during file loading: %s%nException: %s", arg, e));
            }

        });

        // save parsed orders to db
        orders.forEach(orderService::save);

        return orders;
    }

    public void menu() {
        String choice;
        do {
            System.out.println("a. Ilość zamówień łącznie");
            System.out.println("b. Ilość zamówień do klienta o wskazanym identyfikatorze");
            System.out.println("c. Łączna kwota zamówień");
            System.out.println("d. Łączna kwota zamówień do klienta o wskazanym identyfikatorze");
            System.out.println("e. Lista wszystkich zamówień");
            System.out.println("f. Lista zamówień do klienta o wskazanym identyfikatorze");
            System.out.println("g. Średnia wartość zamówienia");
            System.out.println("h. Średnia wartość zamówienia do klienta o wskazanym identyfikatorze");
            System.out.println("exit - quit application");
            System.out.print("\nTell me your choice: ");
            try {
                choice = stdinService.readString();
                switch (choice) {
                    case "a":
                        totalOrderCount();
                        break;
                    case "b":
                        totalOrderCountByClientId();
                        break;
                    case "c":
                        totalOrderPrice();
                        break;
                    case "d":
                        totalOrderPriceByClientId();
                        break;
                    case "e":
                        findAllOrders();
                        break;
                    case "f":
                        findAllOrdersByClientId();
                        break;
                    case "g":
                        averageOrderPrice();
                        break;
                    case "h":
                        averageOrderPriceByClientId();
                        break;
                    case "exit":
                        return;
                    default:
                        System.out.println("Invalid option");
                }
            } catch (Exception ex) {
                System.out.println(ex);
                logger.warn(String.format("Exception during reading from stdin: %s", ex));
            }

            System.out.println();
        } while(true);
    }

    public String saveDialogPrompt() {
        System.out.println("Would you like to save report in a file? [y/n]: ");

        String answer = stdinService.readString();

        if(!answer.equalsIgnoreCase("y")) {
            return "";
        }

        System.out.println("Enter filename: ");
        return stdinService.readString();

    }

    public String saveReport(String filename, String header, Object data) {
        if(!filename.isEmpty()) {
            try {
                StringBuilder sb = new StringBuilder(header);
                sb.append("\n");
                sb.append(data);
                FileUtils.writeStringToFile(new File(filename), sb.toString(), StandardCharsets.UTF_8);
            } catch (Exception ex) {
                logger.warn(String.format("Couldn't write to file: %s", filename));
            }
        }
        return filename;
    }

    public String saveReport(String filename, List<Order> orders) {
        if(!filename.isEmpty()) {
            try {
                FileUtils.write(new File(filename), csvMapper.serialize(orders), StandardCharsets.UTF_8);
            } catch (Exception ex) {
                logger.warn(String.format("Couldn't write to file: %s", filename));
            }
        }

        return filename;
    }
    public String clientIdPrompt() {
        System.out.print("Enter client id: ");

        return stdinService.readString();
    }

    public void totalOrderCount() {
        final long result = orderService.totalOrderCount();
        System.out.println(result);
        saveReport(saveDialogPrompt(), "total_order_count", result);
    }

    public void totalOrderCountByClientId() {
        String clientId = clientIdPrompt();
        final double result = orderService.orderCountByClientId(clientId);
        System.out.println(result);
        saveReport(saveDialogPrompt(), "clientId,total_order_count", clientId + "," + result);
    }

    public void totalOrderPrice() {
        final double result = orderService.totalOrderPrice();
        System.out.println(result);
        saveReport(saveDialogPrompt(), "total_order_price", result);
    }

    public void totalOrderPriceByClientId() {
        String clientId = clientIdPrompt();
        final double result = orderService.orderPriceByClientId(clientId);
        System.out.println(result);
        saveReport(saveDialogPrompt(), "clientId,total_order_price", clientId + "," + result);
    }

    public void findAllOrders() {
        final List<Order> result = orderService.findAllOrders();
        result.forEach(System.out::println);

        saveReport(saveDialogPrompt(), result);
    }

    public void findAllOrdersByClientId() {
        String clientId = clientIdPrompt();
        final List<Order> result = orderService.findOrdersByClientId(clientId);
        result.forEach(System.out::println);

        saveReport(saveDialogPrompt(), result);
    }

    public void averageOrderPrice() {
        final double result = orderService.averageOrderPrice();
        System.out.println(result);
        saveReport(saveDialogPrompt(), "average_order_price", result);
    }

    public void averageOrderPriceByClientId() {
        String clientId = clientIdPrompt();
        final double result = orderService.averageOrderPriceByClientId(clientId);
        System.out.println(result);
        saveReport(saveDialogPrompt(), "clientId,average_order_price", clientId + "," + result);
    }

    @Bean
    @Profile("prod")
    public CommandLineRunner run() {
        return args -> {
            System.out.println("Args: ");
            Arrays.stream(args).forEach(System.out::println);
            loadOrdersFromFiles(args);
            menu();
        };
    }


}
