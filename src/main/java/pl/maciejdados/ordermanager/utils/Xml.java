package pl.maciejdados.ordermanager.utils;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.maciejdados.ordermanager.dto.OrderXmlDto;
import pl.maciejdados.ordermanager.dto.RequestsXmlDto;
import pl.maciejdados.ordermanager.entity.Order;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * Implements simple XML mapper.
 * Facade over Jackson XmlMapper
 *
 * @author Maciej Dados
 */
@Component
@Qualifier("XmlMapper")
public class Xml implements Mapper<Order, OrderXmlDto> {
    private final Logger logger = LoggerFactory.getLogger(Csv.class);

    public String serialize(final List<OrderXmlDto> data) throws IOException {
        XmlMapper mapper = new XmlMapper();
        return mapper.writeValueAsString(new RequestsXmlDto(data));
    }



    public List<Order> deserialize(String str)  {
//        Deserialization could be made a lot simpler by using readValues method but in order to meet tasks
//        requirements it's necessary to read single object at a time

        XmlMapper mapper = new XmlMapper();

        // skip outermost tags
        str = str.replace("<requests>", "");
        str = str.replace("</requests>", "");

        // split every request
        str = str.replace("</request>", "</request>@");
        String[] request = str.split("@");


        return Arrays.stream(request)
                .filter(r -> r.length() > 6) // at least 7 characters are needed to make a valid xml node
                .map(r -> {
                    try {
                        return mapper.readValue(r, OrderXmlDto.class); // read single object
                    } catch (Exception ex) {
                        logger.warn(String.format("Error parsing. Exception: %s", ex));
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .map(OrderXmlDto::toOrder)
                .collect(Collectors.toList());
    }
}
