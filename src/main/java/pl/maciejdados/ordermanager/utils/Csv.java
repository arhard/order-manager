package pl.maciejdados.ordermanager.utils;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.maciejdados.ordermanager.dto.OrderCsvDto;
import pl.maciejdados.ordermanager.entity.Order;
import pl.maciejdados.ordermanager.exception.InvalidCsvColumnException;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Implements simple CSV mapper.
 * Facade over Jackson CsvMapper
 *
 * @author Maciej Dados
 */
@Component
@Qualifier("CsvMapper")
public class Csv implements Mapper<Order, OrderCsvDto> {
    private final Logger logger = LoggerFactory.getLogger(Csv.class);


    public String serialize(final List<OrderCsvDto> data) throws IOException {
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema csvSchema = csvMapper.schemaFor(OrderCsvDto.class).withHeader(); // prepare schema basing on a csv dto, use headers
        return csvMapper.writer(csvSchema).writeValueAsString(data);             // serialize
    }

    public List<Order> deserialize(String str)  {
//        Just as with xml mapper deserialization here could be done much simpler but to meet tasks requirement of reading
//        one object at a time this has to be done this way

        CsvMapper csvMapper = new CsvMapper();
        CsvSchema csvSchema = csvMapper.schemaFor(OrderCsvDto.class);

        // split into lines
        List<String> lines = Arrays.asList(str.split("\n"));

        // header is said to be present every time. If there are less than 2 lines
        // then there is nothing to parse
        if(lines.size() < 2) {
            return Collections.emptyList();
        }

        // take 1st line which is a header
        String header = lines.get(0);

        // count columns
        int columnCount = header.split(",").length;

        // remove first header line, it's no longer needed
        List<String> linesWithoutHeader = lines.subList(1, lines.size());

        // Have to do it line by line to match task requirements
        return linesWithoutHeader.stream()
                .map(line -> {
                    try {
                        // check if line consists of the same amount of columns as header
                        int lineColumnCount = line.split(",").length;

                        if(lineColumnCount != columnCount || columnCount != OrderCsvDto.COLUMN_COUNT) {
                            throw new InvalidCsvColumnException("Invalid amount of columns, expected " + OrderCsvDto.COLUMN_COUNT + " but got " + lineColumnCount);
                        }

                        // map to dto
                        return (OrderCsvDto) csvMapper.readerFor(OrderCsvDto.class).with(csvSchema).readValue(line);
                    } catch (IOException | InvalidCsvColumnException ex) {
                        logger.warn(String.format("Error parsing. Exception: %s", ex));
                    }
                    return null; // it will be later filtered out so there are no NullPointerException
                })
                .filter(Objects::nonNull)
                .map(OrderCsvDto::toOrder) // convert from dto to Order
                .collect(Collectors.toList());
    }
}
