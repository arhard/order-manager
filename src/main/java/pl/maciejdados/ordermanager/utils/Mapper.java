package pl.maciejdados.ordermanager.utils;

import java.io.IOException;
import java.util.List;

/**
 * Interface describing simple object mapper
 *
 * @author Maciej Dados
 */
public interface Mapper<T, D> {
    /**
     * Serializes an object
     * @param object object to be serialized
     * @return serialized String
     * @throws IOException
     */
    String serialize(final List<D> data) throws IOException;

    /**
     * Deserializes a string to an object
     * @param str serialized object in string format
     * @return deserialized list of objects
     */
    List<T> deserialize(String str);
}
